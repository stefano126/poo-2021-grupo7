import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss']
})
export class CatalogoComponent implements OnInit {
  producto: any;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.mostrarProducto().subscribe( data => {
    this.producto = data.producto;
  }

}
