import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CarritoComponent} from './carrito/carrito.component'
import {CatalogoComponent} from './catalogo/catalogo.component'
import {InformacionproductoComponent} from './informacionproducto/informacionproducto.component'
import {InicioComponent} from './inicio/inicio.component'
import {IniciosesionComponent} from "./iniciosesion/iniciosesion.component";
import {MensajefinalComponent} from './mensajefinal/mensajefinal.component'
import {PerfilComponent} from './perfil/perfil.component'
import {RegistroComponent} from './registro/registro.component'
import {SeguimientoComponent} from './seguimiento/seguimiento.component'

const routes: Routes = [
  {path:"carrito", component: CarritoComponent},
  {path:"catalogo", component: CatalogoComponent},
  {path:"informacion_producto", component: InformacionproductoComponent},
  {path:"inicio", component: InicioComponent},
  {path:"inicio_sesion",component:IniciosesionComponent},
  {path:"mensaje_final", component: MensajefinalComponent},
  {path:"perfil", component: PerfilComponent},
  {path:"registro", component: RegistroComponent},
  {path:"seguimiento", component: SeguimientoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
