import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {
  nombres: string = '';
  direccion: string = '';
  codigopostal: string = '';
  numerotelefono: string = '';
  correo: string = '';
  contrasena: string = '';
  constructor() { }

  ngOnInit(): void {
    console.log(this.nombres);
    console.log(this.direccion);
    console.log(this.codigopostal);
    console.log(this.numerotelefono);
    console.log(this.correo);
    console.log(this.contrasena);
  }

}

