export interface Usuario {
    id_usuario: number;
    nombre: string;
    contrasena: number;
    pais: string;
    direccion: string;
    codigo_postal: number;
    correo_electronico: string;
    num_telef: number;
    url_imagen: string;
}
export interface Producto {
    id_producto: number;
    precio: number;
    oferta: number;
    nombre: string;
    descripccion: string;
    lista: Imagen[];
}
export interface Pedido {
    cod_pedido: number;
    id_usuario: number;
    forma_pago: string;
    precio_total: number;
    fecha: number;
    sub_total: number;
    total_impuestos: number;
    estado: string;
}
export interface Detalle_Pedido{
    cod_pedido: number;
    id_producto: number;
    cant_unidades: number;
    precio: number;
}
export interface Categoria {
    codigo_categoria: number;
    nombre_categoria: number;
}
export interface Categoria_Producto {
    codigo_categoria: number;
    id_producto: number;
}
export interface Imagen {
    id_imagen: number;
    url: string;
    id_producto: number;
}