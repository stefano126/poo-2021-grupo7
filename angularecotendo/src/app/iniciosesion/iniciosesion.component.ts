import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-iniciosesion',
  templateUrl: './iniciosesion.component.html',
  styleUrls: ['./iniciosesion.component.scss']
})
export class IniciosesionComponent implements OnInit {
  constructor(api: ApiService, router:Router){ }
  ngOnInit(): void {  
  }
  iniciarSesion():void{
    this.router.navigateByUrl("catalogo");
  }

}
