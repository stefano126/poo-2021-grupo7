import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Pedido } from '../interfaces';

@Component({
  selector: 'app-seguimiento',
  templateUrl: './seguimiento.component.html',
  styleUrls: ['./seguimiento.component.scss']
})
export class SeguimientoComponent implements OnInit {
  mensaje: string = '';
  elementos: string[] = [];
  cantidad: number = 0;
  estado: string | undefined;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.mostrarEstadoPedido().subscribe( data => {
      this.estado = data.estado;

    });
  }

}
