import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { Pedido, Producto } from "./interfaces";
import { retry, catchError } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'})
    export class ApiService {
        obtenerProductos() {
          throw new Error('Method not implemented.');
        }
        httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json;charset=utf-8'
            })
        };
        errorHandl(error: any) {
            let errorMessage = '';
            if (error.error instanceof ErrorEvent) {
                errorMessage = error.error.message;
            } else {
                errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            }
            console.log(errorMessage);
            return throwError(errorMessage);
        }
        constructor(private http: HttpClient) { }
        mostrarProducto():Observable<Producto> {
            return this.http.post<Producto>('http://127.0.0.1:8080/mostrar-producto', null, this.httpOptions)
            .pipe(
                retry(1),
                catchError(this.errorHandl)
            );
        }
        mostrarEstadoPedido():Observable<Pedido> {
            return this.http.post<Pedido>('http://127.0.0.1:8080/mostrar-estado-pedido', null, this.httpOptions)
            .pipe(
                retry(1),
                catchError(this.errorHandl)
            );
        }

    }