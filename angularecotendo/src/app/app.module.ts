import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { IniciosesionComponent } from './iniciosesion/iniciosesion.component';
import { RegistroComponent } from './registro/registro.component';
import { PerfilComponent } from './perfil/perfil.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { InformacionproductoComponent } from './informacionproducto/informacionproducto.component';
import { CarritoComponent } from './carrito/carrito.component';
import { MensajefinalComponent } from './mensajefinal/mensajefinal.component';
import { SeguimientoComponent } from './seguimiento/seguimiento.component';


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    IniciosesionComponent,
    RegistroComponent,
    PerfilComponent,
    CatalogoComponent,
    InformacionproductoComponent,
    CarritoComponent,
    MensajefinalComponent,
    SeguimientoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
