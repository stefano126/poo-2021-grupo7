CREATE DATABASE poo;

USE poo;

CREATE TABLE usuario(
 id_usuario NUMERIC(9) PRIMARY KEY,
 nombre VARCHAR(200),
 contrase�a NUMERIC(9),
 pais VARCHAR(100),
 direccion VARCHAR(200),
 codigo_postal NUMERIC(9),
 correo_electronico VARCHAR(100),
 num_telef NUMERIC(9),
 url_imagen VARCHAR(100)
 );
 
 CREATE TABLE producto(
 id_producto NUMERIC(9) PRIMARY KEY,
 nombre VARCHAR(200),
 precio NUMERIC(9,2),
 oferta NUMERIC(9,2),
 descripcion VARCHAR(800)
 );
 
CREATE TABLE pedido(
 cod_pedido NUMERIC(9) PRIMARY KEY,
 id_usuario NUMERIC(9),
 forma_pago VARCHAR(100),
 precio_total NUMERIC(9,2),
 fecha NUMERIC(9),
 sub_total NUMERIC(9,2),
 total_impuestos NUMERIC(9,2),
 estado VARCHAR(100),
 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
 );
 
CREATE TABLE detalle_pedido(
 cod_pedido NUMERIC(9),
 id_producto NUMERIC(9),
 cant_unidades NUMERIC(9),
 precio NUMERIC(9,2),
 FOREIGN KEY (cod_pedido) REFERENCES pedido(cod_pedido),
 FOREIGN KEY (id_producto) REFERENCES producto(id_producto)
 );
 
CREATE TABLE categoria(
 codigo_categoria NUMERIC(9) PRIMARY KEY,
 nombre_categoria VARCHAR(100)
 );
 
CREATE TABLE categoria_producto(
 codigo_categoria NUMERIC(9),
 id_producto NUMERIC(9),
 FOREIGN KEY (codigo_categoria) REFERENCES categoria(codigo_categoria),
 FOREIGN KEY (id_producto) REFERENCES producto(id_producto)
 );

 CREATE TABLE imagen_producto(
 id_imagen NUMERIC(9),
 url VARCHAR(200),
 id_producto NUMERIC(9),
 FOREIGN KEY (id_producto) REFERENCES producto(id_producto)
 );
 
 --Agregar Producto
 CREATE SEQUENCE secuencia_pruebaaa
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 5;
--Agregar Categoria


--Agregar Producto
create sequence secuencia_producto start with 1 increment by 1 minvalue 1;
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Esponja vegetal",0.10, 0.00, "Esponja perfecta de 7 cm x 10 cm para ser utilizada como esponja de ba�o o para lavar vajillas. Este es un producto zero waste, compostable, sin procesar, libre de pesticidas y 100% vegano.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Toallas higienicas reutilizables",0.15, 0.00, "Tama�o normal de 24 cm. Elaboradas con algodon pyma y telas absorbentes e impermeables para mayor proteccion. No contienen los quimicos da�inos de las toallas higienicas desechables convencionales.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Cepillo de pelo de bambu",79.00, 0.00, "Tama�o 25 cm x 8.5 cm. Es una alternativa zero waste a los cepillos para el cabello hechos con materiales sintaticos. Tiene una sensacion natural y lujosa y funciona exactamente como los cepillos de pelo convencionales. Los alfileres de cepillo son de bambu puro. La base del cojin es de caucho natural. El mango de bambu tiene un acabado de cera de abejas. Envases compostables hechos solo de papel.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Cepillo de dientes de bambu",18.00, 0.00, "Tama�o adulto. Hecho en un 100% a base de plantas. Este es el perfecto remplazo para los cepillos de plastico que tanto contaminan nuestros oceanos. Sus cerdas suaves tienen certificacion USDA basado en plantas.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Desodorante solido",20.00, 0.00, "Peso 50 gr. Acido estearico, aceite de neem, manteca de karita, aceite de coco, harina de arroz, arcilla blanca, Oxido de zinc, piedra de alumbre en polvo, vitamina E y aceites esenciales de Arbol de te, lavanda y naranja.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Dentrifico masticable",16.50, 0.00, "Dentrifico solido hecho a base de insumos naturales que dejara tu aliento fresco y con sabor a menta. Producto vegano y libre de plastico.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Protector solar FPS 30",30.00, 0.00, "Volumen 30 mL. Aceite de coco, aceite de aguaje, manteca de karita, oxido de zinc, cera carnauba y extracto natural de vainilla.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Cuaderno de Bambu",39.50, 0.00, "Tama�o 20 cm x 14 cm. Perfecto para escritores, so�adores y garabateadores.Este elegante cuaderno de bambu esta lleno de 50 hojas de papel 100% reciclado (sin forro).El portatil esta atado con una bobina de metal. Este producto es compostable y sin desperdicio.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Shampoo Manzanilla Suave",30.00, 0.00, "Peso 80 gr. Para cabello delicado, normal o da�ado. Ligero y poderoso. Rejuvenece el cuero cabelludo. Con su accion antioxidante regenera la piel castigada, es anti-inflamatorio y cicatrizante. Ayuda a prevenir y detener la caida del cabello. Repara puntas secas o abiertas. �Deja el pelo sedoso! Enriquecido con Vitamina E y Pro vitamina B5. ");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Shampoo Vita Mandarina",30.00, 0.00, "Peso 80 gr. Para cabello normal, te�ido o rizado. Con aceite de Argan que previene el envejecimiento prematuro, reparando los da�os del pintado y ayuda a tus rizos perfectos. Ademas, contiene aloe vera que regenera, revitaliza y aporta Vitamina B1, para nutrir e hidratar. Ofrece elasticidad y fuerza, logrando un pelo sedoso y brillante. Enriquecido con Vitamina E y Pro vitamina B5.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Shampoo Frutas y Aloe",16.50, 0.00, "Peso 80 gr. Para todo tipo de cabello.�La esencia de un bosque! Para todo tipo de cabello. Con aloe vera que regenera y revitaliza. Aceite de coco organico y almendras dulces que garantizan un cabello brillante, sano y atractivo. La hierbas con el aceite esencial de naranja, mandarina y citronella aportan frescura, son antisepticos y estimulan el crecimiento.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Shampoo Herbal Tea Time",30.00, 0.00, "Peso 80 gr. Para cabello graso, con caspa o caida. El poder de las hierbas le dicen adios a la caspa y grasa. Con aloe vera que regenera y revitaliza y aceite de coco organico y romero que garantiza un cabello brillante y sano. Su aceite de romero es ideal para prevenir la alopecia o caida. Tiene hierbas naturales de romero y aceite esencial de Arbol de te. ");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Acondicionador Manzanilla Keratina",16.50, 0.00, "Peso 63 gr. Para cabello delicado, normal o da�ado. El complemento perfecto para tu shampoo. Un efecto hidratador gracias al aceite organico de coco y cacao. Con keratina vegetal que nutre y reconstruye la fibra capilar ademas de proteger tus laceados. Con su accion antioxidante regenera la piel castigada, es anti-inflamatorio y cicatrizante. Repara puntas secas o abiertas.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Acondicionador Vita Mandarina Keratina",30.00, 0.00, "Peso 63 gr. Para cabello rizado, normal o te�ido. El complemento perfecto para tu shampoo. Un efecto hidratador gracias al aceite organico de coco y cacao. Contiene keratina vegetal especial para reparar puntas. Con aceite de Argan que previene el envejecimiento prematuro, reparando los da�os del pintado. Ademas, contiene aloe vera que regenera, revitaliza y aporta Vitamina B1.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Acondicionador Frutas y Aloe",30.00, 0.00, "Peso 63 gr. Para todo tipo de cabello. El complemento perfecto para tu shampoo. Un efecto hidratador gracias al aceite organico de coco y cacao. Con aloe vera que regenera y revitaliza. Aceite de almendras dulces que garantiza un cabello brillante, sano y atractivo. Su aceite esencial de naranja, mandarina y citronella aportan frescura, son antisepticos y estimulan el crecimiento.");
insert into producto(id_producto, nombre, precio, oferta, descripcion) values(nextval(secuencia_producto), "Acondicionador Herbal Tea Time",16.50, 0.00, "Peso 63 gr. Para cabello graso o con caspa. El complemento perfecto para tu shampoo. Un efecto hidratador gracias al aceite organico de coco y cacao. Con aloe vera que regenera y revitaliza. Tiene aceite de romero que garantiza un cabello brillante y sano. Enriquecido con aceites esenciales de romero, ylang ylang y arbol de te.");


 
 
 SELECT p.id_producto,p.precio,p.oferta,p.nombre,p.descripcion,p.cantidad,i.id_imagen,i.url
 FROM producto p
 JOIN imagen_producto i ON (i.id_producto=p.id_producto)  

SELECT * FROM producto;