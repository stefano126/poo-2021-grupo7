package proyectofinal.ecotendo.servicios;

import proyectofinal.ecotendo.dto.Detalle_Pedido;
import proyectofinal.ecotendo.dto.Pedido;
import proyectofinal.ecotendo.dto.Producto;
import proyectofinal.ecotendo.dto.Usuario;

public interface InterfServicio {
    void agregar_usuario(Usuario usuario);
    void eliminar_usuario(Usuario usuario);
    void modificar_usuario(Usuario usuario, String tipo_cambio, String cambio);
    Usuario mostrarUsuario(int id_usuario);

    void agregar_producto(Producto producto);
    void eliminar_producto(Producto producto);
    void modificar_producto(Producto producto, String tipo_cambio, String cambio);
    void agregar_oferta(Producto producto,int oferta);
    void eliminar_oferta(Producto producto);
    Producto mostrarProducto(int id_producto);

    Pedido mostrarPedido(int cod_pedido);
    String mostrarEstado(Pedido pedido);
    void modificarEstado(Pedido pedido);
    float mostrarPrecioTotal(Pedido pedido);
    void agregarPedido(Pedido pedido);

    void agregarDetallePedido(Detalle_Pedido detallePedido);
    Detalle_Pedido mostrarDetallePedido(int cod_pedido, int id_producto);
}
