package proyectofinal.ecotendo.servicios;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import proyectofinal.ecotendo.dao.ImplDaoDetallePedido;
import proyectofinal.ecotendo.dao.ImplDaoPedido;
import proyectofinal.ecotendo.dao.ImplDaoProducto;
import proyectofinal.ecotendo.dao.ImplDaoUsuario;
import proyectofinal.ecotendo.dto.Detalle_Pedido;
import proyectofinal.ecotendo.dto.Pedido;
import proyectofinal.ecotendo.dto.Producto;
import proyectofinal.ecotendo.dto.Usuario;

@Service
@Transactional
public class Servicio implements InterfServicio{
    @Autowired
    private ImplDaoUsuario daoUsuario;
    private ImplDaoProducto daoProducto;
    private ImplDaoPedido daoPedido;
    private ImplDaoDetallePedido daoDetallePedido;

    //Servicios de USUARIO

    public void agregar_usuario(Usuario usuario) {
        daoUsuario.agregar_usuario(usuario);
    }

    public void eliminar_usuario(Usuario usuario) {
        daoUsuario.eliminar_usuario(usuario);
    }

    public void modificar_usuario(Usuario usuario, String tipo_cambio, String cambio) {
        daoUsuario.modificar_usuario(usuario, tipo_cambio, cambio);
    }

    public Usuario mostrarUsuario(int id_usuario) {
        return daoUsuario.mostrarUsuario(id_usuario);
    }

    //Servicios de PRODUCTO

    public void agregar_producto(Producto producto) {
        daoProducto.agregar_producto(producto);  
    }

    public void eliminar_producto(Producto producto) {
        daoProducto.eliminar_producto(producto);
    }

    public void modificar_producto(Producto producto, String tipo_cambio, String cambio) {
        daoProducto.modificar_producto(producto, tipo_cambio, cambio);
    }

    public void agregar_oferta(Producto producto, int oferta) {
        daoProducto.agregar_oferta(producto, oferta);
    }

    public void eliminar_oferta(Producto producto) {
        daoProducto.eliminar_oferta(producto);
    }

    public Producto mostrarProducto(int id_producto) {
        return daoProducto.mostrarProducto(id_producto);
    }

    //Servicios de PEDIDO

    public Pedido mostrarPedido(int cod_pedido) {
        return daoPedido.mostrarPedido(cod_pedido);
    }

    public String mostrarEstado(Pedido pedido) {
        return daoPedido.mostrarEstado(pedido);
    }

    public void modificarEstado(Pedido pedido) {   
        daoPedido.modificarEstado(pedido);     
    }

    public float mostrarPrecioTotal(Pedido pedido) {
        return daoPedido.mostrarPrecioTotal(pedido);
    }

    public void agregarPedido(Pedido pedido) {
        daoPedido.agregarPedido(pedido);
    }

    //Servicios de DETALLE_PEDIDO

    public void agregarDetallePedido(Detalle_Pedido detallePedido) {
        daoDetallePedido.agregarDetallePedido(detallePedido);        
    }

    public Detalle_Pedido mostrarDetallePedido(int cod_pedido, int id_producto) {
        return daoDetallePedido.mostrarDetallePedido(cod_pedido, id_producto);
    }


}
