package proyectofinal.ecotendo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcotendoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcotendoApplication.class, args);
	}

}
