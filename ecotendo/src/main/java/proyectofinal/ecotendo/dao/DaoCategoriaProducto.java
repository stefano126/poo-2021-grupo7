package proyectofinal.ecotendo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Categoria_Producto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class DaoCategoriaProducto implements ImplDaoCategoriaProducto {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();            }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
       
    }

    public void agregarproductocategoria(Categoria_Producto categoria_producto) {
        String sql = "insert into categoria_producto(codigo_categoria,id_producto) values(?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, categoria_producto.getCodigo_categoria());
            sentencia.setInt(2, categoria_producto.getId_producto());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
    }
        
    public Categoria_Producto converCategoria_Producto(ResultSet resultado) throws SQLException{
        Categoria_Producto Categoria_Producto = new Categoria_Producto(
            resultado.getInt("codigo_categoria"), 
            resultado.getInt("id_producto")
            );
            return Categoria_Producto;
    }

    public Categoria_Producto mostrarcodigoCategoria(int codigo_categoria) {
        
            String sql = "select categoria_producto.codigo_categoria, categoria_producto.id_producto\n"+
            "from categoria_producto\n"+
            "where categoria_producto.codigo_categoria=?";
            Categoria_Producto Categoria_Producto = null;
            
            try {
                obtenerConexion();
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setInt(1, codigo_categoria);
                ResultSet resultado = sentencia.executeQuery();
                while(resultado.next()){
                    Categoria_Producto = converCategoria_Producto(resultado);
                }
                cerrarConexion(resultado, sentencia);
            } catch(SQLException throwables) {
                throwables.printStackTrace();
            }
            return Categoria_Producto;
        
        
    }

    
}
