package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Usuario;

public interface ImplDaoUsuario {

    void agregar_usuario(Usuario usuario);
    void eliminar_usuario(Usuario usuario);
    void modificar_usuario(Usuario usuario, String tipo_cambio, String cambio);
    Usuario mostrarUsuario(int id_usuario);
    
}
