package proyectofinal.ecotendo.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Producto;

@Repository
public class DaoProducto implements ImplDaoProducto{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public void agregar_producto(Producto producto) {
        String sql = "insert into producto(id_producto,nombre, precio, descripcion) values(nextval(secuencia_producto),?,?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, producto.getNombre());
            sentencia.setFloat(2, producto.getPrecio());
            sentencia.setString(3, producto.getDescripcion());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
    }

    public void eliminar_producto(Producto producto) {
        String sql = "delete from producto where id_producto = ?";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, producto.getId_producto());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void modificar_producto(Producto producto, String tipo_cambio, String cambio) {
        String sql = "update producto set "+ tipo_cambio +" ="+cambio+" \n"+
        "where id_producto = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, producto.getId_producto());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void agregar_oferta(Producto producto, int oferta) {
        String sql = "update producto set oferta ="+oferta+" \n"+
        "where id_producto = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, producto.getId_producto());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void eliminar_oferta(Producto producto) {
        String sql = "update producto set oferta = 0 \n"+
        "where id_producto = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, producto.getId_producto());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        
    }

    public Producto converProducto(ResultSet resultado) throws SQLException{
        Producto producto = new Producto(
            resultado.getInt("id_producto"),
            resultado.getString("nombre"), 
            resultado.getFloat("precio"), 
            resultado.getString("descripcion")
            );
            return producto;
    }

    public Producto mostrarProducto(int id_producto) {
        String sql=" select p.id_producto, p.nombre, p.precio, p.descripcion\n" +
        " from producto p\n" +
        " where p.producto = ?";
        
        Producto producto = null;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, id_producto);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                producto = converProducto(resultado);
            }
            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return producto;
    }
    
}
