package proyectofinal.ecotendo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Pedido;

@Repository
public class DaoPedido implements ImplDaoPedido{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Pedido converPedido(ResultSet resultado) throws SQLException{
        Pedido pedido = new Pedido(
            resultado.getInt("cod_pedido"), 
            resultado.getInt("id_usuario"), 
            resultado.getString("forma_pago"), 
            resultado.getFloat("precio_total"), 
            resultado.getInt("fecha"), 
            resultado.getFloat("sub_total"),
            resultado.getFloat("total_impuestos"),
            resultado.getString("estado")
        );
        return pedido;
    }

    public Pedido mostrarPedido(int cod_pedido) {
        String sql=" select pe.cod_pedido, pe.id_usuario, pe.forma_pago, pe.precio_total, pe.fecha, pe.sub_total, pe.total_impuestos, pe.estado\n"+
        " from pedido pe\n" +
        " where pe.cod_pedido = ?";
        
        Pedido pedido = null;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, cod_pedido);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                pedido = converPedido(resultado);
            }
            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return pedido;
    }

    public String mostrarEstado(Pedido pedido) {
        String sql=" select pe.estado\n"+
        " from pedido pe\n" +
        " where pe.cod_pedido = ?";
        
        String estado=null;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, pedido.getCod_pedido());
            ResultSet resultado = sentencia.executeQuery();
            
            while(resultado.next()){
                pedido = converPedido(resultado);
            }

            estado=pedido.getEstado();

            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return estado;

    }

    public void modificarEstado(Pedido pedido) {
        String sql = "update pedido set\n "+
        "where cod_pedido = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, pedido.getCod_pedido());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public float mostrarPrecioTotal(Pedido pedido) {
        String sql=" select pe.precio_total\n"+
        " from pedido pe\n" +
        " where pe.cod_pedido = ?";
        
        float precio_total=0;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, pedido.getCod_pedido());
            ResultSet resultado = sentencia.executeQuery();
            
            while(resultado.next()){
                pedido = converPedido(resultado);
            }

            precio_total=pedido.getPrecio_total();

            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return precio_total;
    }

    public void agregarPedido(Pedido pedido){
        String sql = "insert into pedido(cod_pedido, id_usuario, forma_pago, precio_total, fecha, sub_total, total_impuestos, estado) values(nextval(secuencia_pedido),?,?,?,?,?,?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, pedido.getId_usuario());
            sentencia.setString(2, pedido.getForma_pago());
            sentencia.setFloat(3, pedido.getPrecio_total());
            sentencia.setInt(4, pedido.getFecha());
            sentencia.setFloat(5, pedido.getSub_total());
            sentencia.setFloat(6, pedido.getTotal_impuestos());
            sentencia.setString(7, pedido.getEstado());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
    };
    
}
