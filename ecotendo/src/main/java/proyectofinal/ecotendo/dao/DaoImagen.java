package proyectofinal.ecotendo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Imagen;

import java.sql.*;

@Repository
public class DaoImagen implements ImplDaoImagen{
   @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();            }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        
    }

    public void agregarimagenproducto(Imagen imagen) {
        String sql = "insert into imagen(idImagen,url,idProducto) values(?,?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, imagen.getIdImagen());
            sentencia.setString(2, imagen.getUrl());
            sentencia.setInt(3, imagen.getIdProducto());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
        
    }

    public Imagen converImagen(ResultSet resultado) throws SQLException{
        Imagen imagen = new Imagen(
            resultado.getInt("idImagen"),
            resultado.getString("url"), 
            resultado.getInt("idProducto")
            );
            return imagen;
    }

    @Override
    public Imagen mostraridimagen(int idImagen) {
        String sql=" select imagen.idImagen, imagen.url, imagen.idProducto\n" +
        " from imagen\n" +
        " where imagen.idImagen = ?";
        
        Imagen imagen = null;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, idImagen);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                imagen = converImagen(resultado);
            }
            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return imagen;
    }

    
}
