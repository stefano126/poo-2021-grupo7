package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Categoria_Producto;

public interface ImplDaoCategoriaProducto {

    void agregarproductocategoria(Categoria_Producto categoria_producto);

    Categoria_Producto mostrarcodigoCategoria(int codigo_categoria);
   
}
