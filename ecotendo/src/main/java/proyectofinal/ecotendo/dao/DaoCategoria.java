package proyectofinal.ecotendo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Categoria;



@Repository
public class DaoCategoria implements ImplDaoCategoria{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();            }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        
    }

    public void agregar_categoria(Categoria categoria) {
        String sql = "insert into categoria(codigo_categoria,nombre_categoria) values(?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, categoria.getNombre_categoria());
            sentencia.setInt(2, categoria.getCodigo_categoria());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
    }
    
    public Categoria converCategoria(ResultSet resultado) throws SQLException{
        Categoria categoria = new Categoria(
            resultado.getInt("codigo_categoria"), 
            resultado.getString("nombre_categoria")
            );
            return categoria;
    }

    public Categoria mostrarnombreCategoria(String nombre_categoria) {
        String sql = "select categoria.codigo_categoria, categoria.nombre_categoria\n"+
        "from categoria\n"+
        "where categoria.nombre_categoria=?";
        Categoria categoria = null;
        
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, nombre_categoria);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                categoria = converCategoria(resultado);
            }
            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return categoria;
    
    }
}