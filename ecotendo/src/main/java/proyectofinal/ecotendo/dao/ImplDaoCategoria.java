package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Categoria;

public interface ImplDaoCategoria {
    
    Categoria mostrarnombreCategoria(String nombre_categoria);
    void agregar_categoria(Categoria categoria);
  
}
