package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Producto;

public interface ImplDaoProducto {
    void agregar_producto(Producto producto);
    void eliminar_producto(Producto producto);
    void modificar_producto(Producto producto, String tipo_cambio, String cambio);
    void agregar_oferta(Producto producto,int oferta);
    void eliminar_oferta(Producto producto);
    Producto mostrarProducto(int id_producto);
}
