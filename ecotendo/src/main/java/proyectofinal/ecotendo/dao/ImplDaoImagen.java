package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Imagen;


public interface ImplDaoImagen {

    void agregarimagenproducto(Imagen imagen);

    Imagen mostraridimagen (int idImagen);
   
}
