package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Detalle_Pedido;

public interface ImplDaoDetallePedido {
    void agregarDetallePedido(Detalle_Pedido detallePedido);
    Detalle_Pedido mostrarDetallePedido(int cod_pedido, int id_producto);
}
