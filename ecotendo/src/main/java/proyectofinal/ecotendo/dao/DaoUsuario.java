package proyectofinal.ecotendo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Usuario;

@Repository
public class DaoUsuario implements ImplDaoUsuario {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();            }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void agregar_usuario(Usuario usuario) {
        String sql = "insert into usuario(id_usuario, nombre, contrasena, pais, direccion, codigo_postal, correo_electronico, num_telef, url_imagen) values(nextval(secuencia_usuario),?,?,?,?,?,?,?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario.getNombre());
            sentencia.setString(2, usuario.getContrasena());
            sentencia.setString(3, usuario.getPais());
            sentencia.setString(4, usuario.getDireccion());
            sentencia.setInt(5, usuario.getCodigo_postal());
            sentencia.setString(6, usuario.getCorreo_electronico());
            sentencia.setInt(7, usuario.getNum_telef());
            sentencia.setString(8, usuario.getUrl_imagen());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
    }

    public void eliminar_usuario(Usuario usuario) {
        String sql = "delete from usuario where id_usuario = ?";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, usuario.getId_usuario());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void modificar_usuario(Usuario usuario, String tipo_cambio, String cambio) {
        String sql = "update usuario set "+ tipo_cambio +" ="+cambio+" \n"+
        "where id_usuario = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, usuario.getId_usuario());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        
    }

    public Usuario converUsuario(ResultSet resultado) throws SQLException{
        Usuario usuario = new Usuario(
            resultado.getInt("id_usuario"), 
            resultado.getString("nombre"), 
            resultado.getString("contrasena"), 
            resultado.getString("pais"), 
            resultado.getString("direccion"), 
            resultado.getInt("codigo_postal"), 
            resultado.getString("correo_electronico"), 
            resultado.getInt("num_telef"), 
            resultado.getString("url_imagen")
            );
            return usuario;
    }

    public Usuario mostrarUsuario(int id_usuario) {
        String sql=" select u.id_usuario, u.nombre, u.contrasena, u.pais, u.direccion, u.codigo_postal,\n" +
        "       u.correo_electronico, u.num_telef, u.url_imagen\n" +
        " from usuario u\n" +
        " where p.id_usuario = ?";
        
        Usuario usuario = null;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, id_usuario);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                usuario = converUsuario(resultado);
            }
            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
    
}