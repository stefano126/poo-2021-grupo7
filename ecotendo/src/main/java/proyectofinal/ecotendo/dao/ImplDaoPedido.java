package proyectofinal.ecotendo.dao;

import proyectofinal.ecotendo.dto.Pedido;

public interface ImplDaoPedido {
    Pedido mostrarPedido(int cod_pedido);
    String mostrarEstado(Pedido pedido);
    void modificarEstado(Pedido pedido);
    float mostrarPrecioTotal(Pedido pedido);
    void agregarPedido(Pedido pedido);
}