package proyectofinal.ecotendo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import proyectofinal.ecotendo.dto.Detalle_Pedido;

@Repository
public class DaoDetallePedido implements ImplDaoDetallePedido{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void agregarDetallePedido(Detalle_Pedido detallePedido) {
        String sql = "insert into detallePedido(cod_pedido,id_producto, cant_unidades, precio) values(nextval(secuencia_usuario),?,?,?)";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, detallePedido.getId_producto());
            sentencia.setFloat(2, detallePedido.getCant_unidades());
            sentencia.setFloat(3, detallePedido.getPrecio());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }
        
    }

    public Detalle_Pedido converDetalle_Pedido(ResultSet resultado) throws SQLException{
        Detalle_Pedido detalle_Pedido = new Detalle_Pedido(
            resultado.getInt("cod_pedido"), 
            resultado.getInt("id_producto"), 
            resultado.getInt("cant_unidades"),
            resultado.getFloat("precio")
        );
        return detalle_Pedido;
    }

    public Detalle_Pedido mostrarDetallePedido(int cod_pedido, int id_producto) {
        String sql=" select mp.cod_pedido, mp.id_producto, mp.cant_unidades, mp.precio\n" +
        " from detalle_pedido mp\n" +
        " where mp.cod_pedido = ?, mp.id_producto = ?";
        
        Detalle_Pedido detalle_pedido = null;
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, cod_pedido);
            sentencia.setInt(2, id_producto);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                detalle_pedido = converDetalle_Pedido(resultado);
            }
            cerrarConexion(resultado, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
        return detalle_pedido;
    }
    
}
