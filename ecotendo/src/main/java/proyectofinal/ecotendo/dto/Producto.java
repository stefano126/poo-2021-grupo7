package proyectofinal.ecotendo.dto;

import lombok.Data;

@Data
public class Producto {
    private int id_producto;
    private String nombre;
    private float precio;
    private int oferta=0;
    private String descripcion;
    private Imagen imagen;

    public Producto(int id_producto, float precio, int oferta, String nombre, String descripccion) {
        this.id_producto = id_producto;
        this.precio = precio;
        this.oferta = oferta;
        this.nombre = nombre;
        this.descripcion = descripccion;
    }

    public Producto(int id_producto, String nombre, float precio, String descripccion) {
        this.id_producto = id_producto;
        this.precio = precio;
        this.nombre = nombre;
        this.descripcion = descripccion;
    }

    public int getId_producto() {
        return id_producto;
    }
    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }
    public float getPrecio() {
        return precio;
    }
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public int getOferta() {
        return oferta;
    }
    public void setOferta(int oferta) {
        this.oferta = oferta;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripccion) {
        this.descripcion = descripccion;
    }

	public Imagen getImagen() {
		return imagen;
	}

	public void setImagen(Imagen imagen) {
		this.imagen = imagen;
	} 
}
