package proyectofinal.ecotendo.dto;

import lombok.Data;

@Data
public class Categoria_Producto{
    int codigo_categoria;
    int id_producto;

    
    public Categoria_Producto(int codigo_categoria, int id_producto) {
        this.codigo_categoria = codigo_categoria;
        this.id_producto = id_producto;
    }
    public int getCodigo_categoria() {
        return codigo_categoria;
    }
    public void setCodigo_categoria(int codigo_categoria) {
        this.codigo_categoria = codigo_categoria;
    }
    public int getId_producto() {
        return id_producto;
    }
    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

}