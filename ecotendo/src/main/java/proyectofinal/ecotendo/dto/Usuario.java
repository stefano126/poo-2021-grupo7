package proyectofinal.ecotendo.dto;

import lombok.Data;

@Data
public class Usuario{
    private int id_usuario;
    private String nombre;
    private String contrasena;
    private String pais;
    private String direccion;
    private int codigo_postal;
    private String correo_electronico;
    private int num_telef;
    private String url_imagen;


    
    public Usuario(int id_usuario, String nombre, String contrasena, String pais, String direccion, int codigo_postal,
            String correo_electronico, int num_telef, String url_imagen) {
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.contrasena = contrasena;
        this.pais = pais;
        this.direccion = direccion;
        this.codigo_postal = codigo_postal;
        this.correo_electronico = correo_electronico;
        this.num_telef = num_telef;
        this.url_imagen = url_imagen;
    }

    public Usuario(){
    }

    public Usuario(String correo_electronico, String contrasena){
        this.correo_electronico= correo_electronico;
        this.contrasena = contrasena;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(int codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public int getNum_telef() {
        return num_telef;
    }

    public void setNum_telef(int num_telef) {
        this.num_telef = num_telef;
    }

    public String getUrl_imagen() {
        return url_imagen;
    }

    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }
}