package proyectofinal.ecotendo.dto;

import lombok.Data;

@Data
public class Pedido {
    int cod_pedido;
    int id_usuario;
    String forma_pago;
    float precio_total;
    int fecha;
    float sub_total;
    float total_impuestos;
    String estado;
    
    public Pedido(int cod_pedido, int id_usuario, String forma_pago, float precio_total, int fecha, float sub_total,
            float total_impuestos, String estado) {
        this.cod_pedido = cod_pedido;
        this.id_usuario = id_usuario;
        this.forma_pago = forma_pago;
        this.precio_total = precio_total;
        this.fecha = fecha;
        this.sub_total = sub_total;
        this.total_impuestos = total_impuestos;
        this.estado = estado;
    }

    public Pedido(int cod_pedido) {
        this.cod_pedido = cod_pedido;
    }

    public int getCod_pedido() {
        return cod_pedido;
    }

    public void setCod_pedido(int cod_pedido) {
        this.cod_pedido = cod_pedido;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getForma_pago() {
        return forma_pago;
    }

    public void setForma_pago(String forma_pago) {
        this.forma_pago = forma_pago;
    }

    public float getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(float precio_total) {
        this.precio_total = precio_total;
    }

    public int getFecha() {
        return fecha;
    }

    public void setFecha(int fecha) {
        this.fecha = fecha;
    }

    public float getSub_total() {
        return sub_total;
    }

    public void setSub_total(float sub_total) {
        this.sub_total = sub_total;
    }

    public float getTotal_impuestos() {
        return total_impuestos;
    }

    public void setTotal_impuestos(float total_impuestos) {
        this.total_impuestos = total_impuestos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    

}