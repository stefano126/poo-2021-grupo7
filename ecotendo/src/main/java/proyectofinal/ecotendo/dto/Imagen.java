package proyectofinal.ecotendo.dto;

import lombok.Data;

@Data
public class Imagen {
   private Integer idImagen;
   private String url;
   private Integer idProducto;

   
public Imagen(Integer idImagen, String url, Integer idProducto) {
	this.idImagen = idImagen;
	this.url = url;
	this.idProducto = idProducto;
}
public Integer getIdImagen() {
	return idImagen;
}
public void setIdImagen(Integer idImagen) {
	this.idImagen = idImagen;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public Integer getIdProducto() {
	return idProducto;
}
public void setIdProducto(Integer idProducto) {
	this.idProducto = idProducto;
}
}
