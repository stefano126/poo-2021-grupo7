package proyectofinal.ecotendo.dto;

import lombok.Data;

@Data
public class Detalle_Pedido {
    int cod_pedido;
    int id_producto;
    int cant_unidades;
    float precio;
    
    public Detalle_Pedido(int cod_pedido, int id_producto, int cant_unidades, float precio) {
        this.cod_pedido = cod_pedido;
        this.id_producto = id_producto;
        this.cant_unidades = cant_unidades;
        this.precio = precio;
    }

    public int getCod_pedido() {
        return cod_pedido;
    }

    public void setCod_pedido(int cod_pedido) {
        this.cod_pedido = cod_pedido;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getCant_unidades() {
        return cant_unidades;
    }

    public void setCant_unidades(int cant_unidades) {
        this.cant_unidades = cant_unidades;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}