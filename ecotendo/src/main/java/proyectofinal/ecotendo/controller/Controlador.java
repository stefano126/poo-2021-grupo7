package proyectofinal.ecotendo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import proyectofinal.ecotendo.dto.Detalle_Pedido;
import proyectofinal.ecotendo.dto.Pedido;
import proyectofinal.ecotendo.dto.Producto;
import proyectofinal.ecotendo.dto.Usuario;
import proyectofinal.ecotendo.servicios.Servicio;


@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;

    //Pedido
    @RequestMapping(
        value = "/mostrar-pedido",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Pedido mostrarPedido(@RequestBody int cod_pedido){
        return servicio.mostrarPedido(cod_pedido);
    }
    @RequestMapping(
        value = "/mostrar-estado-pedido",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody String mostrarEstado(@RequestBody Pedido pedido){
        return servicio.mostrarEstado(pedido);
    }
    @RequestMapping(
        value = "/modificar-estado-pedido",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void modificarEstado(@RequestBody Pedido pedido){
        servicio.modificarEstado(pedido);
    }
    @RequestMapping(
        value = "/mostrar-precio-total",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Float mostrarPrecioTotal(@RequestBody Pedido pedido){
        return servicio.mostrarPrecioTotal(pedido);
    }
    @RequestMapping(
        value = "/agregar-pedido",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void agregarPedido(@RequestBody Pedido pedido){
        servicio.agregarPedido(pedido);
    }

    @RequestMapping(
        value = "/agregar-detalle-pedido",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void agregarDetallePedido(@RequestBody Detalle_Pedido detalle_Pedido){
        servicio.agregarDetallePedido(detalle_Pedido);
    }
    @RequestMapping(
        value = "/mostrar-detalle-pedido",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Detalle_Pedido mostrarDetallePedido(@RequestBody int cod_pedido, int id_producto){
        return servicio.mostrarDetallePedido(cod_pedido,id_producto);
    }


    //Usuario
    @RequestMapping(
        value = "/agregar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void agregarusuario(@RequestBody Usuario usuario){
        servicio.agregar_usuario(usuario);
    }

    @RequestMapping(
        value = "/eliminar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void eliminarusuario(@RequestBody Usuario usuario){
        servicio.eliminar_usuario(usuario);
    }
    @RequestMapping(
        value = "/modificar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void modificarusuario(@RequestBody Usuario usuario, String tipo_cambio, String cambio){
        servicio.modificar_usuario(usuario, tipo_cambio, cambio);
    }
    @RequestMapping(
        value = "/mostrar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void mostrarusuario(@RequestBody int id_usuario){
        servicio.mostrarUsuario(id_usuario);
    }


    //Producto
    @RequestMapping(
        value = "/agregar-producto",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void agregarProducto(@RequestBody Producto producto){
        servicio.agregar_producto(producto);
    }
    
    @RequestMapping(
        value = "/eliminar-producto",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void eliminarProducto(@RequestBody Producto producto){
        servicio.eliminar_producto(producto);
    }

    @RequestMapping(
        value = "/modificar-producto",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void modificarProducto(@RequestBody Producto producto, String tipo_cambio, String cambio){
        servicio.modificar_producto(producto, tipo_cambio, cambio);
    }

    @RequestMapping(
        value = "/agregar-oferta",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void agregarOferta(@RequestBody Producto producto, int oferta){
        servicio.agregar_oferta(producto, oferta);
    }

    @RequestMapping(
        value = "/eliminar-oferta",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void eliminarOferta(@RequestBody Producto producto){
        servicio.eliminar_oferta(producto);
    }

    @RequestMapping(
        value = "/mostrar-producto",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Producto mostrarProducto(@RequestBody int id_producto){
        return servicio.mostrarProducto(id_producto);
    }

}
